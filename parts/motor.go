package parts

import (
	"errors"
	"github.com/shipp02/go-rov/msg"
	"gobot.io/x/gobot/drivers/gpio"
	"gobot.io/x/gobot/platforms/firmata"
	"log"
	"strconv"
)

// Message convention topic:motor_topic@rpm
// Motor Speed Message
type MotorMessage struct {
	Percent uint8
}

// Motor Actor controls motor Speed
type Motor struct {
	Describe string
	topic    string
	sd       *gpio.ServoDriver
}

func (motor *Motor) Receive(m msg.IPubSub) {
	ms := m.(*msg.PubSub)
	if i, err := strconv.Atoi(ms.Messages[0]); err == nil {
		err := motor.sd.Move(uint8(i))
		if err != nil {
			log.Fatal("Motor error: ", err)
		}
	}
}

func (motor *Motor) Topics() string {
	return "motor-" + motor.topic
}

//  Topic must be globally unique for that MotorAdapter
func NewMotor(des, topic string) *Motor {
	m := &Motor{
		Describe: des,
		topic:    topic,
	}
	f := firmata.NewAdaptor("/dev/cu.usbmodem")
	m.sd = gpio.NewServoDriver(f, "3")
	return m
}

type MotorAdapter struct {
	Motors []Motor
	Topic  string
}

func NewMotorAdapter() MotorAdapter {
	return NewNamedMotorAdapter("motor:")
}

func NewNamedMotorAdapter(topic string) MotorAdapter {
	return MotorAdapter{
		Motors: make([]Motor, 0, 5),
		Topic:  topic,
	}
}

func (ma MotorAdapter) Add(m Motor) error {
	if len(ma.Motors) == cap(ma.Motors) {
		replace := make([]Motor, 0, len(ma.Motors)*2)
		copy(replace, ma.Motors)
	}
	for _, oldM := range ma.Motors {
		if oldM.topic == m.topic {
			return errors.New("topic of motor is clashing with topic: " + oldM.topic)
		}
	}
	ma.Motors[len(ma.Motors)] = m
	return nil
}
