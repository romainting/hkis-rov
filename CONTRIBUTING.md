You need the go version 1.11 or greater. In addition you need to install pkg-config for the C dependencies.

**Dependencies:**
 - libzmq
 - libczmq
 - libsodium

MacOS install instructions:

Toolchain:

``$ brew install go pkg-config``

Dependencies:

``$ brew install zeromq czmq libsodium``



